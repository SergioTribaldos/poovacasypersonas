/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Scanner;

/**
 *
 * @author 1DAM
 */
public class POO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      Scanner sc=new Scanner(System.in);
       
        Persona p1=new Persona();
        p1.nombre="Miguel";
        p1.edad=32;
        p1.altura=1.75f;
        p1.apellidos="Paramos";
        p1.puesto="ordeño";
        p1.sueldo=1250;
        p1.misVacas=new Vaca[3];
  
        Persona p2=new Persona();
        p2.nombre="Lola";
        p2.apellidos="Mento";
        p2.edad=32;
        p2.altura=1.25f;
        p2.puesto="carniceria";
        p2.sueldo=1000;
        p2.misVacas=new Vaca[3];
        
        Persona p3=new Persona();
        p3.nombre="Peter";
        p3.apellidos="Griffin";
        p3.edad=55;
        p3.altura=1.91f;
        p3.puesto="gerente";
        p3.sueldo=1500;
        p3.misVacas=new Vaca[3];
        
        Vaca v1=new Vaca();
        v1.edad=12;
        v1.litrosOrdeñados=new float[3];
            v1.litrosOrdeñados[0]=12;
            v1.litrosOrdeñados[1]=5;
            v1.litrosOrdeñados[2]=8;
        v1.nombre="Goku";
        v1.numeroSerie=001;
        v1.peso=300;
        v1.asignaFuncion();
        
        Vaca v2=new Vaca();
        v2.edad=3;
        v2.litrosOrdeñados=new float[3];
            v2.litrosOrdeñados[0]=1;
            v2.litrosOrdeñados[1]=20;
            v2.litrosOrdeñados[2]=11;
        v2.nombre="Vegeta";
        v2.numeroSerie=002;
        v2.peso=230;
        v2.asignaFuncion();
        
        Vaca v3=new Vaca();
        v3.edad=7;
        v3.litrosOrdeñados=new float[3];
            v3.litrosOrdeñados[0]=10;
            v3.litrosOrdeñados[1]=2;
            v3.litrosOrdeñados[2]=33;
        v3.nombre="Trunks";
        v3.numeroSerie=003;
        v3.peso=98;
        v3.asignaFuncion();
        
        Vaca vac[]=new Vaca[3];
        vac[0]=v1;
        vac[1]=v2;
        vac[2]=v3;
       
        Persona listaDeEmpleados[]=Persona.creaPersonas();            /////////FUNCION IMPORTANTE!!!!!!!!!
        System.out.println(Persona.imprimeArray(listaDeEmpleados));     
        
       
                
        
        //////////////////////////////////////////////////////////////
        /*
        System.out.println(p2.imprimePersona());       /////////la persona p1 pasa por parametro automaticamente
        p2.proporcionSueldo(p1);
                
        
        System.out.println(p2.cuantoGana());
        p2.ajustaSueldoA(p1);
        System.out.println(p2.cuantoGana());
        
        p1.quedarmeVacas(vac);
        p1.cuantasVacasTengo();
        
        System.out.println(v1.datosVaca());
*/
    }
    
    
    
    
}
