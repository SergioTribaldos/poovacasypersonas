/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Scanner;

/**
 *
 * @author 1DAM
 */
public class Persona {
    
    
    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca misVacas[];
    
    
    public String imprimePersona(){                        //////////la clase p1  pasa por parametro, al indicarlo en el main(p1.imprime...)
         return "\n"+"-------------------\n"+
                "Nombre y Apellidos: "+this.nombre+" "+this.apellidos+"\n"+
                "Altura: "+this.altura+"m \n"+
                "Edad: "+this.edad+" años\n"+
                "Puesto: "+this.puesto+"\n"+
                "Sueldo: "+this.sueldo+" €"+
                "\n-------------------\n"; 
    }
   
    /**
     * Dice si la persona que llama a la funcion es mayor de una cierta edad
     * @param int la edad con la qe vamos a compsarar a la persona
     * @return true si es mayor, false si es menor
     */  
    public boolean esMaryorDe(int edad){
        
        return this.edad>edad;      
        
    }
    
    public String cuantoGana(){
        return "Gana "+this.sueldo+"€";
    }
    
    public void proporcionSueldo(Persona per){
        System.out.println("La diferencia de sueldo entre "+per.nombre+" y "+this.nombre+" es de: "+(per.sueldo/this.sueldo)*100+"%"); 
    }    
    
    public float ajustaSueldoA(Persona per){
        if(this.puesto.equals("ordeño")&&per.puesto.equals("gerencia")){
           return this.sueldo=per.sueldo*0.5f;           
        }
        if(this.puesto.equals("ordeño")&&per.puesto.equals("carniceria")){
           return this.sueldo=per.sueldo*1.25f;           
        }
        if(this.puesto.equals("carniceria")&&per.puesto.equals("gerencia")){
           return this.sueldo=per.sueldo*0.25f;           
        }
        if(this.puesto.equals("carniceria")&&per.puesto.equals("ordeño")){
           return this.sueldo=per.sueldo*0.75f;           
        }
        if(this.puesto.equals("gerencia")&&per.puesto.equals("ordeño")){
           return this.sueldo=per.sueldo*1.5f;           
        }
        if(this.puesto.equals("gerencia")&&per.puesto.equals("carniceria")){
          return  this.sueldo=per.sueldo*1.75f;           
        }else{
           return this.sueldo=this.sueldo;
        }           
    }
    
    public void quedarmeVacas(Vaca[]vac){
        this.misVacas=new Vaca[vac.length];
        int contador=0;
        for(int i=0;i<vac.length;i++){
            if(this.puesto.equals(vac[i].funcion)){
             this.misVacas[contador]=vac[i];
             contador++;
        }
            
        }   
    }
    
    public void cuantasVacasTengo(){
        System.out.print("Las vacas que tiene "+this.nombre+" son: ");
        for(int i=0;i<misVacas.length;i++){
            if(this.misVacas[i]==null){
                break;
            }else{
              System.out.print(this.misVacas[i].nombre+" ");  
            }
            
        }
    }    
    
    public static String imprimeArray(Persona[]per){
        String datos="";
        for(int i=0;i<per.length;i++){
            datos+="\n"+"-------------------\n"+
                "Nombre y Apellidos: "+per[i].nombre+" "+per[i].apellidos+"\n"+
                "Altura: "+per[i].altura+"m \n"+
                "Edad: "+per[i].edad+" años\n"+
                "Puesto: "+per[i].puesto+"\n"+
                "Sueldo: "+per[i].sueldo+" €"+
                "\n-------------------\n"; 
            
        }
        return datos;
    }
    
    public static Persona[] creaPersonas(){
        /////////////////////CREAR PERSONAS DENTRO DE UN BUCLE////////////////////////////////////////////
        Scanner sc=new Scanner(System.in);
        System.out.println("Cuantas personas quieres crear?");
        int cantidadPersonas=sc.nextInt();
        sc.nextLine();
        Persona ap[]=new Persona[cantidadPersonas];           ///////////podemos hacer un array de Persona, cada posicion es una persona
        
        for(int i=0;i<ap.length;i++){    
            System.out.println("INTRODUCE DATOS DE LA PERSONA Nº"+(i+1));
            ap[i]=new Persona();                      ///////////DE ESTA MANERA CREAMOS UNA NUEVA PERSONA EN CADA ITERACION Y LA PONEMOS EN LA POSICION DEL ARRAY CORRESPONDIENTE
            System.out.println("Introduce nombre");
            ap[i].nombre=sc.nextLine();
            System.out.println("Introduce apellidos");
            ap[i].apellidos=sc.nextLine();
            System.out.println("Introduce altura");
            ap[i].altura=sc.nextFloat();
            sc.nextLine();
            System.out.println("Introduce edad");
            ap[i].edad=sc.nextInt();
            sc.nextLine();
            System.out.println("Introduce puesto");
            ap[i].puesto=sc.nextLine();
            System.out.println("Introduce sueldo");
            ap[i].sueldo=sc.nextInt();
            sc.nextLine();
            System.out.println(" ");
            
           
        }
        
        return ap;
        
    }
}
